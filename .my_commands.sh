#!bin/bash


function create() {
    BASE_FOLDER="/home/mibkr/Documents/Projects"
    PYTHON_SCRIPT_FOLDER="/home/mibkr/Documents/Projects/project_initializer"
    cd 
    echo "Current project folder is ${BASE_FOLDER}"
    echo "Python script folder is ${PYTHON_SCRIPT_FOLDER}"
    while test $# -gt 0; do
           case "$1" in
                -t | --type)
                    shift
                    GITIGNORE_TYPE=$1
                    shift
                    ;;
                -h | --help)
                    shift
                    echo "Available"
                    shift
                    ;;
                -*)
                   shift
                   echo "Not a valid flag. Use -t for .gitignore type."
                   shift
                   ;;
                * | -o | --Output)
                   PROJECT="${1}"
                   PROJECT_FOLDER="${BASE_FOLDER}/${1}"
                   shift
                   ;;
          esac
  done 
    if [ -z "${GITIGNORE_TYPE}" ]
    then
        echo "No .gitignore type provided. e.g. -t Python or -t VisualStudio"            
    fi  
    cd
    echo "Creating folder ${PROJECT_FOLDER} and repository."
    cd ${PYTHON_SCRIPT_FOLDER}
    source Env/bin/activate  
    echo "Environment on"
    python3 -m pip install -r ${PYTHON_SCRIPT_FOLDER}/requirements.txt
    PYTHON_OUTPUT=$(python bitbucket.py ${BASE_FOLDER} ${PROJECT} ${GITIGNORE_TYPE})
    echo -e $PYTHON_OUTPUT
    deactivate
    cd
    echo "Environment off"
    cd
    cd "${PROJECT_FOLDER}"
    git init
    git remote add origin git@bitbucket.org:MiBKr/${PROJECT,,}.git
    touch README.md    
    git add .
    git commit -m "Initial commit"
    git push -u origin master
    echo Repo Initialized 
    cd "${PROJECT_FOLDER}"
    code .
}
