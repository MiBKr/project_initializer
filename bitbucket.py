import requests
from requests.auth import HTTPBasicAuth
import sys
import os
import errno
import json


username = 'mibkr'
password = 'K!ng0fP!gs'
# Should use a different method but oh whatever

def create_project_folder(project_folder):
    try:
        os.mkdir(project_folder)
        return True
    except OSError as exc:
        print("Folder Exists! Creation cancelled.  ")
        return False

    pass
        


def create_bitbucket_repo(repo):
    try:
        url = f"https://api.bitbucket.org/2.0/repositories/mibkr/{repo}"
        r = requests.post(url, auth=HTTPBasicAuth(username, password))  
        return True
    except ConnectionError as exc:
        print("Connection error. Failed to create repo. \n")
        return False


def get_gitignore(target_folder, gitignore):
    try:
        r = requests.get("https://api.github.com/gitignore/templates")
    except ConnectionError as exc:
        print("Connection error. Failed to get gitignore. \n")
        return False
    if(r.status_code ==200):
        ignore_types = json.loads(r.text)
        for _type in ignore_types:
            if _type.lower() == gitignore.lower():
                r = requests.get(f"https://raw.githubusercontent.com/github/gitignore/master/{_type}.gitignore")  
                with open(os.path.join(target_folder, ".gitignore"), 'wb') as f:
                    f.write(r.content)
                
                print(f"{_type} gitignore added to project folder.")
                return True

        print("GitIgnore type not found.")
        return False
    else:
        print("Bad request. Failed to get gitignore. ")
        return False


if __name__ == "__main__":
    base_folder = str(sys.argv[1])
    repo = str(sys.argv[2])
    gitignore = str(sys.argv[3]) if len(sys.argv) == 4 else None  
    target_folder = os.path.join(base_folder,repo)
    folder_created = create_project_folder(target_folder)
    if folder_created:
        create_bitbucket_repo(repo)
        if gitignore != None:
            get_gitignore(target_folder, gitignore)
        
        print("Success.")
        os.sys.exit(0)
    else:        
        os.sys.exit(1)
    

